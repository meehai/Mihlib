import numpy as np
from .layer import Layer
from ..transfer_function import Identity

class ConvolutionalLayer(Layer):
	def __init__(self, inputSize, kernelSize, stride, numKernels, transferFunction=Identity):
		self.depth, self.width, self.height = inputSize
		assert (self.height - kernelSize) % stride == 0 and (self.width - kernelSize) % stride == 0
		self.kernelSize = kernelSize
		self.stride = stride
		self.numKernels = numKernels

		self.heightOutput = int((self.height - kernelSize) / stride + 1)
		self.widthOutput = int((self.width - kernelSize) / stride + 1)
		super().__init__(inputSize, (self.numKernels, self.widthOutput, self.heightOutput), transferFunction)

		# Layer's parameters
		self.weights = np.random.normal(0, np.sqrt(2 / (self.depth * kernelSize * kernelSize)), \
			(numKernels, self.depth, kernelSize, kernelSize))
		self.biases = np.random.normal(0, np.sqrt(2 / (self.depth * kernelSize * kernelSize)), (numKernels, 1))

		# Computed value
		self.layerOutput = np.zeros(self.outputSize)

		# Gradients
		self.gradientWeights = np.zeros((self.numKernels, self.depth, self.kernelSize, self.kernelSize))
		self.gradientBiases = np.zeros((self.numKernels, 1))

	def forward(self, layerInput):
		self.layerOutput = np.zeros((layerInput.shape[0], *self.outputSize))
		for i in range(layerInput.shape[0]):
			self.layerOutput[i] = self.forward_simple(layerInput[i])
		return self.transferFunction.apply(self.layerOutput)

	# Method using numpy tricks, version 1. Tested to have same results as 6 for version.
	def forward_simple(self, layerInput):
		layerOutput = np.zeros(self.outputSize)

		# TODO: think of mgrid.
		for i in range(self.heightOutput):
			out_i = i * self.stride
			for j in range(self.widthOutput):
				out_j = j * self.stride
				# A is the little portion of the image (on all dimensions) that the kernel is applied to
				A = layerInput[:, out_i : out_i + self.kernelSize, out_j : out_j + self.kernelSize]
				res = np.sum(A * self.weights, axis=(1,2,3)) + self.biases.reshape(self.numKernels)
				layerOutput[:, i, j] = res

		# Apply the trasnfer function to the output
		return layerOutput

	def backward(self, layerInput, propagatedError):
		result = np.zeros(layerInput.shape)
		for i in range(layerInput.shape[0]):
			result[i] = self.backward_simple(layerInput[i], propagatedError[i], self.layerOutput[i])
		return result

	# Method using numpy tricks, version 1. Tested to have same results as 6 for version.
	def backward_simple(self, layerInput, propagatedError, layerOutput):
		# delta_Y is the actual error after deriving the forward's output with the trasnfer function. This is
		#  equivalent to having a TanH layer that translates the error from previous step but inside the conv layer.
		delta_y = propagatedError * self.transferFunction.applyDerivative(layerOutput)
		result = np.zeros(layerInput.shape)

		# Gradient w.r.t weights
		for out_i in range(self.heightOutput):
			in_i = out_i * self.stride
			for out_j in range(self.widthOutput):
				in_j = out_j * self.stride
				A_weights = layerInput[:, in_i : in_i + self.kernelSize, in_j : in_j + self.kernelSize]
				A_delta = result[:, in_i : in_i + self.kernelSize, in_j : in_j + self.kernelSize]
				b = delta_y[:, out_i, out_j]

				for n in range(self.numKernels):
					self.gradientWeights[n, :, :, :] += A_weights * b[n]
					A_delta[:, :] += self.weights[n, :, :, :] * b[n]

		self.gradientBiases += np.sum(delta_y, axis=(1,2)).reshape((-1, 1))
		return result

	# Used to access the weights and bias values (such as in the process of backpropagation)
	def getWeights(self):
		return self.weights

	def getBias(self):
		return self.biases

	def getGradientWeights(self):
		return self.gradientWeights

	def getGradientBias(self):
		return self.gradientBiases

	def setWeights(self, value):
		self.weights = value

	def setBias(self, value):
		self.biases = value

	def setGradientWeights(self, value):
		self.gradientWeights = value

	def setGradientBias(self, value):
		self.gradientBiases = value

	def hasParameters(self):
		return True

	def __str__(self):
		return "Convolution (NumPy-1 version) " + super().__str__() + " (" + self.transferFunction.__str__() + ")"
import numpy as np
from .layer import Layer

class MaxPoolLayer(Layer):
	def __init__(self, inputSize, stride):
		self.depth, self.width, self.height = inputSize
		assert(self.height % stride == 0 and self.width % stride == 0)
		self.widthOutput = int(self.width / stride)
		self.heightOutput = int(self.height / stride)
		super().__init__(inputSize, (self.depth, self.widthOutput, self.heightOutput))

		self.stride = stride
		self.allMaxIndexes = None

	def forward(self, layerInput):
		layerOutput = np.zeros((layerInput.shape[0], *self.outputSize))
		self.allMaxIndexes = []
		for N in range(layerInput.shape[0]):
			maxIndexes = {}
			for m in range(self.depth):
				for i in range(self.heightOutput):
					for j in range(self.widthOutput):
						A = layerInput[N, m, i * self.stride : (i + 1) * self.stride, \
							j * self.stride : (j + 1) * self.stride]
						out_i, out_j = np.array(np.unravel_index(A.argmax(), A.shape))
						layerOutput[N, m, i, j] = np.max(A)
						maxIndexes[(m, out_i + i * self.stride, out_j + j * self.stride)] = (m, i, j)
			self.allMaxIndexes.append(maxIndexes)
		return layerOutput

	def backward(self, layerInput, propagatedError):
		result = np.zeros((layerInput.shape[0], *self.inputSize))
		for N in range(layerInput.shape[0]):
			thisResult = result[N]
			maxIndexes = self.allMaxIndexes[N]
			thisError = propagatedError[N]
			for key, value in maxIndexes.items():
				thisResult[key] = thisError[value]
		return result

	def __str__(self):
		return "MaxPool " + super().__str__()
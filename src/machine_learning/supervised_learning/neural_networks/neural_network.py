# feed_forward.py - Implementation of a feed forward network and multiple layers.
import numpy as np
import joblib
from copy import deepcopy

class NeuralNetwork:
	def __init__(self, layersInfo, errorFunction, optimizer):
		self.layers = deepcopy(layersInfo)
		self.optimizer = optimizer
		self.errorFunction = errorFunction()

		# Check consistency
		outputSize = self.layers[0].outputSize
		for i in range(1, len(self.layers)):
			assert outputSize == self.layers[i].inputSize, "Wrong shape: output[l-1]: " + str(outputSize) \
				+ " vs. input[l]: " + str(self.layers[i].inputSize)
			outputSize = self.layers[i].outputSize

		self.inputSize = self.layers[0].inputSize
		self.outputSize = self.layers[len(self.layers) - 1].outputSize

	def forward(self, networkInput):
		raise NotImplementedError("Should have implemented this")

	def backward(self, networkInput, networkError):
		raise NotImplementedError("Should have implemented this")

	def train(self, data, labels, epochsCount, batchSize, epoch_callbacks, iteration_callbacks,
		iteration_callbacks_count):
		raise NotImplementedError("Should have implemented this")

	def test(self, data, labels):
		raise NotImplementedError("Should have implemented this")

	def optimize(self):
		for layer in self.layers:
			if layer.hasParameters():
				self.optimizer.optimize(layer)

	def printWeights(self):
		print("Weights:")
		for layer in self.layers:
			if type(layer.weights) != float:
				print(layer.__str__() + " Min:", np.min(layer.weights), "Max:", np.max(layer.weights), \
					"Mean:", np.mean(layer.weights))

	def __str__(self):
		Str = "Error function: " + self.errorFunction.__str__() + "\n"
		Str += "Optimizer: " + self.optimizer.__str__() + "\n"
		Str += "Layers: \n"
		for i in range(len(self.layers)):
			Str += str(i) + " " + self.layers[i].__str__() + "\n";
		return Str

class FeedForward(NeuralNetwork):
	def forward(self, networkInput):
		lastInput = networkInput
		for layer in self.layers:
			lastInput = layer.forward(lastInput)
		return lastInput

	def backward(self, networkInput, networkError):
		currentError = networkError
		# Traverse them backward
		for i in range(len(self.layers) - 1, 0, -1):
			currentLayer = self.layers[i]
			previousLayer = self.layers[i - 1]
			currentError = currentLayer.backward(previousLayer.layerOutput, currentError)
		self.layers[0].backward(networkInput, currentError)

	# TODO: see test_Forward_train_2 for a more generic output (not just supervised training with labels)
	# This version expects the layer to handle batch data (MxDx1, for example for FC). For 1 item: (1xDx1) is expected.
	def train(self, data, labels, epochsCount, batchSize, epoch_callbacks=[], iteration_callbacks=[],
		iteration_callbacks_count=None):
		numData = len(data)
		print("Training. Num epochs:", epochsCount, "Iterations:", numData // batchSize, "Batch size:", batchSize)

		for epoch in range(epochsCount):
			permutation = np.random.permutation(numData)
			for i in range(0, numData, batchSize):
				iterationCallbacksCheck = False

				numInput = batchSize if i + batchSize <= numData else numData - batchSize
				networkInput = data[permutation[i : i + numInput]]
				outputLabels = labels[permutation[i : i + numInput]]

				networkOutput = self.forward(networkInput)
				outputLabels = outputLabels.reshape(networkOutput.shape)
				error = self.errorFunction.computeErrorMinimum(outputLabels, networkOutput)

				self.backward(networkInput, error)
				self.optimize()

				# Iteration callbacks
				if (iteration_callbacks_count != None) and ((i // batchSize) % iteration_callbacks_count) == 0:
					# gives remaining for correct index printing: 1996 -> 4, 2000 -> 0 if count is 1000
					for callback in iteration_callbacks:
						callback(self, epoch, i)

			# Epoch callbacks
			for callback in epoch_callbacks:
				callback(self, epoch)

	def test(self, data, labels):
		assert len(data) == len(labels)
		numData = len(data)
		correctCount = 0

		# Save the results for error calculation afterwards.
		networkOutputs = self.forward(data)
		# This can be MxNx1 vs MxN for example, but still correct.
		networkOutputs = networkOutputs.reshape(labels.shape)

		outputArgMax = np.argmax(networkOutputs, axis=1)
		labelsArgMax = np.argmax(labels, axis=1)
		correctCount = len(np.where(outputArgMax - labelsArgMax == 0)[0])

		error = self.errorFunction.computeCurrentError(labels, networkOutputs)
		accuracy = correctCount / numData
		return accuracy, error

	def fit(self, data):
		forwardPass = self.forward(data)
		return np.argmax(forwardPass, axis=1).reshape((data.shape[0], )).astype(int)

	def save(self, fileName):
		joblib.dump(self, fileName)

	def __str__(self):
		Str = "Feed Forward network. \n"
		Str += super().__str__()
		return Str


# shapes.py Module used for adding shapes to images, such as squares (bounding boxes), lines, circles, ellipses etc.
#  Every such shape has a function that adds one element and a function that adds a list of elements 
#  (addSquares vs addSquare).
import numpy as np
from utilities.constants import *
from utilities.others import flrange, minMax
from mathematics.linear_algebra.lines import getLineParams
from .others import isPointInImage

# Adds a line in the image, defined by 2 points.
# Method: remove_outliers - the 2 points must be inside the image. remove - plot only the points that intersect the image plane.
def addLine(image, p1, p2, color=(255, 0, 0), method="remove_outliers", copy=True):
	if method == "strict":
		assert isPointInImage(p1, image.shape) and isPointInImage(p2, image.shape)

	if p1[X] == p2[X] and p1[Y] == p2[Y]:
		print("[addLine] The 2 pints are identical, returning original image.")
		return image

	newImage = np.copy(image) if copy == True else image
	a, b, c = getLineParams(p1, p2)
	numSteps = max(abs(p1[X] - p2[X]), abs(p1[Y] - p2[Y])) + 1
	# Vertical line, so all X's are the same.
	if b == 0:
		y = flrange(p1[Y], p2[Y], numSteps)
		x = np.zeros((numSteps, )) - (c / a)
	else:
		x = flrange(p1[X], p2[X], numSteps)
		y = -(a * x + c) / b
	if method == "remove_outliers":
		a = np.logical_or(y < 0, y >= image.shape[I])
		b = np.logical_or(x < 0, x >= image.shape[J])
		where = np.where(np.logical_or(a == True, b == True))
		y = np.delete(y, where)
		x = np.delete(x, where)
	newImage[y.astype(np.int), x.astype(np.int)] = color
	return newImage

# Given a list of points (p1, p2), draw N lines.
def addLines(image, points, color=(255, 0, 0), method="remove_outliers", copy=True):
	newImage = np.copy(image) if copy == True else image
	for p1, p2 in points:
		newImage = addLine(newImage, p1, p2, color, method=method, copy=False)
	return newImage

# Internal function
# Given an image, and a pair of points (top_left, bottom_right), construct the squares that contains the two points.
# TODO: use smart indexing and addLine
def addSquare(image, square, color=(255, 0, 0)):
	pct1 = square[0]
	pct2 = square[1]
	(min_i, max_i) = minMax(pct1[0], pct2[0])
	for pixel_i in range(min_i, max_i + 1):
		if pixel_i < 0 or pixel_i >= image.shape[I]:
			continue
		if pct1[1] >= 0 and pct1[1] < image.shape[J]:
			image[pixel_i][pct1[1]] = color
		if pct2[1] >= 0 and pct2[1] < image.shape[J]:
			image[pixel_i][pct2[1]] = color

	(min_j, max_j) = (min(pct1[1], pct2[1]), max(pct1[1], pct2[1]))
	for pixel_j in range(min_j, max_j + 1):
		if pixel_j < 0 or pixel_j >= image.shape[J]:
			continue
		if pct1[0] >= 0 and pct1[0] < image.shape[I]:
			image[pct1[0]][pixel_j] = color
		if pct2[0] >= 0 and pct2[0] < image.shape[I]:
			image[pct2[0]][pixel_j] = color
	return image

def addCircle(image, indices, color=(255, 0, 0), copy=True):
	new_image = np.copy(image) if copy == True else image
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			for (R, center) in indices:
				D = euclidean_distance(center, (i,j))
				if abs(D - R) < 1:
					new_image[i][j] = color
					break
	return new_image

def addEllipse(image, indices, color=(255, 0, 0), copy=True):
	new_image = np.copy(image) if copy == True else image
	for i in range(image.shape[I]):
		for j in range(image.shape[J]):
			for (F1, F2, R) in indices:
				D = euclidean_distance(F1, (i,j)) + euclidean_distance(F2, (i,j))
				if abs(D - R) < 1:
					new_image[i][j] = color
					break
	return new_image

# Adds the line of a square to the image, given the top left and bottom right coordinates
# indices = [((x_left, y_left), (x_right, y_right)), ..., () ]
# Given an image and a list of sets of points [(top_left, bottom_right)], construct the squares.
def addSquares(image, squares, color=(255, 0, 0), copy=True):
	new_image = np.copy(image) if copy == True else image
	for i in range(len(squares)):
		new_image = addSquare(new_image, squares[i], color)
	return new_image

# Given an image, a list of points (center_position) and a list of radiuses, construct the squares.
# indices = (positions, radius)
def addCenterSquares(image, centers, radiuses, color=(255, 0, 0), copy=True):
	assert(len(centers) == len(radiuses))
	new_image = np.copy(image) if copy == True else image
	for i in range(len(centers)):
		center = centers[i]
		size = radiuses[i]
		#assert(center[0] >= 0 and center[0] < image.shape[I] and center[1] >= 0 and center[1] < image.shape[J])
		new_image[center[0]][center[1]] = color

		# Compute all the inner squares from radius=1 to radius=size+1
		for i in range(1, size+1):
			top_left = (max(center[0] - i, 0), max(center[1] - i, 0))
			bottom_right = (min(center[0] + i, image.shape[I]), min(center[1] + i, image.shape[J]))
			new_image = addSquare(new_image, (top_left, bottom_right), color)
	return new_image
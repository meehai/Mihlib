#plotter.py Module that implements different kinds of plots (images, points, histograms etc.)
# TODO - Think of making this a class and a possibilty to add the plotting to a new thread so it doesn't stop the
#  program when you call plt.show
import matplotlib.pyplot as plt
import numpy as np
from scipy import misc
from random import randint
from .constants import *
from mathematics.linear_algebra.lines import getLineParams_mb, getLineParams
from utilities.others import flrange, isNumber

# wrapper for each sub plot in a figure. The figure itself must be careful with subplots not overlapping.
class SubPlot:
	def __init__(self, axes):
		self.axes = axes

		self.requiresLegend = False
		self.numItems = 0

	def updateKwargs(self, kwargs, numItems):
		colors = ["r", "g", "b", "c", "m", "y", "k", "w"]
		items = [("label", None), ("line_interpolation", False)]

		for item, defaultValue in items:
			if not item in kwargs:
				kwargs[item] = defaultValue

		if not "color" in kwargs:
			kwargs["color"] = colors[numItems % len(colors)]

		if "label" in kwargs and kwargs["label"] != None:
			self.requiresLegend = True
		self.numItems += 1
		return kwargs

	# plots random data and interpolates the points linearly if said so (useful for plotting functions)
	def plot_data(self, data, **kwargs):
		kwargs = self.updateKwargs(kwargs, self.numItems)
		assert len(data) in (2,)

		lineInterpolation = kwargs.pop("line_interpolation")
		if lineInterpolation == True:
			newItem = plt.Line2D(*data, **kwargs)
			self.axes.add_line(newItem)
		else:
			newItem = self.axes.scatter(*data, **kwargs)

	# @param line Equation of the line (a, b, c)
	# @param limits The limits of the line
	def plot_line(self, line, limits=(-10, 10), **kwargs):
		kwargs = self.updateKwargs(kwargs, self.numItems)
		a, b, c = line

		# If a (-10, 10) limit is given, we must infer if this is X or Y coordinate
		if isNumber(limits[0]):
			# This will work for both cases below, so we avoid more ifs
			p1, p2 = (limits[0], limits[0]), (limits[1], limits[1])
		else:
			assert len(limits[0]) == 2, "Expected a (p1, p2) limit or a (number, number) limit"
			p1, p2 = limits

		if b == 0:
			y = flrange(p1[Y], p2[Y], 10)
			x = np.zeros((10, )) - (c / a)
		else:
			x = flrange(p1[X], p2[X], 10)
			y = -(a * x + c) / b
		kwargs["line_interpolation"] = True
		self.plot_data((x, y), **kwargs)

	# plots a line between 2 points
	def plot_line_2points(self, p1, p2, **kwargs):
		kwargs = self.updateKwargs(kwargs, self.numItems)
		assert p1[X] != p2[X] or p1[Y] != p2[Y]

		self.plot_points((p1, p2), **kwargs)
		self.plot_line(getLineParams(p1, p2), limits=((p1, p2)), **kwargs)

	# plots N points with no linear interpolation between them (normal scatter)
	def plot_points(self, points, **kwargs):
		kwargs = self.updateKwargs(kwargs, self.numItems)
		assert len(points[0]) == 2

		points = np.array(points)
		kwargs["line_interpolation"] = False
		self.plot_data((points[:, X], points[:, Y]), **kwargs)

	# plots a polygon, given by N consecutive vertices
	def plot_polygon(self, points, **kwargs):
		kwargs = self.updateKwargs(kwargs, self.numItems)
		assert len(points) >= 3

		for i in range(len(points) - 1):
			self.plot_line_2points(points[i], points[i + 1], **kwargs)
		self.plot_line_2points(points[-1], points[0], **kwargs)

	# Unless this is called, calling plt.show() will have no effect for this figure.
	def plot(self):
		if self.requiresLegend:
			self.axes.legend()
		self.axes.plot()

# wrapper for each matplotlib figure. Each figure has a list of subplots it operates on.
class Figure:
	# gridShape The shape of the subplots. By default, there is just a 1x1 grid (1 figure), but this can be changed.
	def __init__(self, gridShape=(1, 1)):
		# Still use the pyplot's figure object and work around it.
		self.figure = plt.figure()
		self.subPlots = np.zeros(gridShape, dtype=np.object)
		self.gridShape = gridShape

		for i in range(gridShape[I]):
			for j in range(gridShape[J]):
				axes = self.figure.add_subplot(*gridShape, i * gridShape[i] + j + 1)
				self.subPlots[i, j] = SubPlot(axes)

	def at(self, i, j):
		return self.subPlots[i, j]

	# Plot all the subplots of this figure
	def plot(self, block=True):
		for subplot in self.subPlots.flatten():
			subplot.plot()

	# Since most of the time we are working with just one subplot, calling the object will return the subplot.
	# This is equivalent to fig.at(0, 0)
	def __call__(self):
		assert self.gridShape == (1, 1), "Can only use this method if just one subplot is availabe"
		return self.subPlots[0, 0]

# wrapper on top of pyplot's functionality. It contains a list of figures.
class Plotter:
	def __init__(self):
		# A list of all matplotlib figures.
		self.figures = []
		pass

	def addFigure(self, figure):
		self.figure = figure

	# Plot all the figures
	def plot(self):
		for figure in self.figures:
			figure.plot()

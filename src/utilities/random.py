# random.py Wrappers on top of different RNG strategies.
from .metaclasses import Singleton
import random
import numpy as np

# calling this twice guarantees to return a different number every time.
class UniqueRandint(metaclass=Singleton):
	def __init__(self):
		self.last = None
		random.seed()

	def __call__(self, left, right):
		assert right - left >= 1
		r = random.randint(left, right)
		while r == self.last:
			r = random.randint(left, right)
		self.last = r
		return r

# usage uniqueRandint(left, right)
uniqueRandint = UniqueRandint()

# Given a set of data and a number, pick n unique items from the data
def randomPick(data, n, return_index=False):
	assert n < len(data)
	picked_index = []
	picked_data = []

	for i in range(n):
		while True:
			value = uniqueRandint(0, len(data) - 1)
			if not value in picked_index:
				picked_index.append(value)
				picked_data.append(data[value])
				break
	if return_index:
		return picked_data, picked_index
	return picked_data

# Samples from the uniform distribution values in [0, 1]
def uniformSample(count):
	return np.random.uniform(0, 1, size=(count, ))
import matplotlib.pyplot as plt
import numpy as np
from scipy import misc
from random import randint
from .constants import *
from mpl_toolkits.mplot3d import Axes3D
import os

from mathematics.linear_algebra.lines import getLineParams_mb

# Global figures for all plotting
lib_figures = []

# Method that creates a new figure, adds title (or any other arguments that I might come up with)
# Similar to a parent constructor in OOP, needed for all plottings in current stage.
def plot_base(**kwargs):
	kwargs["figsize"] = None if not "figsize" in kwargs else kwargs["figsize"]

	# Add a new figure or add the first figure.
	if not "new_figure" in kwargs or kwargs["new_figure"] == True or len(lib_figures) == 0:
		lib_figures.append(plt.figure(figsize=kwargs["figsize"]))

	Projection = None if not "projection" in kwargs else kwargs["projection"]
	if Projection == "2d":
		Projection = "rectilinear"
	# If Axis is a different number, ([0] - no. rows, [1] - no. columns, [2] - index in the grid)
	Axis = (1, 1, 1) if not "axis" in kwargs else kwargs["axis"]
	# Send each parameters as individual parameter
	lib_figures[-1].add_subplot(*Axis, projection=Projection)

	# Disable axis or not (defualt not)
	ShowAxis = "on" if not "show_axis" in kwargs else ("on" if kwargs["show_axis"] is True else "off")
	lib_figures[-1].gca().axis(ShowAxis)

	if "title" in kwargs:
		lib_figures[-1].gca().set_title(kwargs["title"])

	if "xlim" in kwargs:
		lib_figures[-1].gca().set_xlim(kwargs["xlim"])

	if "ylim" in kwargs:
		lib_figures[-1].gca().set_ylim(kwargs["ylim"])

	if "xticks" in kwargs:
		lib_figures[-1].gca().set_xticks(kwargs["xticks"])

	if "yticks" in kwargs:
		lib_figures[-1].gca().set_yticks(kwargs["yticks"])

	if "xlabel" in kwargs:
		lib_figures[-1].gca().set_xlabel(kwargs["xlabel"])

	if "ylabel" in kwargs:
		lib_figures[-1].gca().set_ylabel(kwargs["ylabel"])

	# TODO
	# if "label" in kwargs:
		# lib_figures[-1].gca().set_label(kwargs["label"])		

# Creates a histogram based on the data you are given and the range of bins.
def plot_hist(data, bins, **kwargs):
	plot_base(**kwargs)
	lib_figures[-1].gca().hist(data, bins)

# Given an image, plot it rgb or greyscale (according to dimensions)
def plot_image(data, **kwargs):
	plot_base(**kwargs)

	cmap = kwargs["cmap"] if "cmap" in kwargs else ("gray" if len(data.shape) == 2 else None) 
	plt.imshow(np.array(misc.toimage(data)), cmap=cmap)

# Given a set of points [(x, y, [z])] and a Range=[(x_min, x_max), (y_min, y_max)], plot those points on a new figure.
def plot_points(data, Range=None, **kwargs):
	assert(len(data) > 0 and len(data[0]) in (1,2,3))
	plot_base(**kwargs)
	if Range != None:
		assert(len(Range) in [1, 2, 3])

		# 2D
		# Plot based on the ranges given for X=(x_min, x_max)
		assert(len(Range[X]) == 2)
		lib_figures[-1].gca().set_xlim(xmin=Range[X][0], xmax=Range[X][1])

		# If we also have values for Y=(y_min, y_max), plot based on this too, otherwise it's detected by min/max 
		# values of the data
		if len(Range) > Y:
			assert(len(Range[Y]) == 2)
			lib_figures[-1].gca().set_ylim(ymin=Range[Y][0], ymax=Range[Y][1])

		if len(Range) > Z:
			assert(len(Range[Z]) == 3)
			lib_figures[-1].gca().set_zlim(zmin=Range[Z][0], zmax=Range[Z][1])

	if not "color" in kwargs:
		kwargs["color"] = "r"

	if not "marker" in kwargs:
		kwargs["marker"] = "o"

	projection_type = lib_figures[-1].gca().name
	if projection_type == "rectilinear":
		lib_figures[-1].gca().scatter(data[:,X], data[:,Y], marker=kwargs["marker"], color=kwargs["color"])
	elif projection_type == "3d":
		lib_figures[-1].gca().scatter(data[:,X], data[:,Y], data[:,Z], marker=kwargs["marker"], \
			color=kwargs["color"])

# Given a line equation of form y = mx + b or ax + by + c = 0, plot that line on the current figure (add plot_base?).
# TODO: plot 3d lines
def plot_line(equation, Range, Range_step=0.05, **kwargs):
	assert(len(Range) == 2)
	plot_base(**kwargs)
	x = np.arange(Range[0], Range[1], Range_step)
	if len(equation) == 2:
		m, b = equation
		if m == 0:
			y = np.ones(x.shape) * b
		else:
			y = m * x + b
	elif len(equation) == 3:
		a, b, c = equation
		# TODO see what to do for cases of a or b == 0
		assert a != 0 and b != 0
		y = (-a / b) * x - (c / b)
	else:
		assert False

	# TODO: move in base
	label = kwargs["label"] if "label" in kwargs else None
	if "label" in kwargs:
		lib_figures[-1].gca().plot(x, y, label=kwargs["label"])
	else:
		lib_figures[-1].gca().plot(x, y)

def plot_line_2points(p1, p2, **kwargs):
	equation = getLineParams_mb(p1, p2)
	minX = min(p1[X], p2[X])
	maxX = max(p1[X], p2[X])
	plot_line(equation, (minX, maxX), **kwargs)

def plot_clusters(data, clusters, centroids):
	K = len(centroids)

	projection = str(len(data[0])) + "d" # 2d / 3d
	plot_base(new_figure=True, projection=projection)

	for i in range(K):
		cluster_points = data[np.where(clusters == i)]
		randomColor = (randint(0, 255) / 255, randint(0, 255) / 255, randint(0, 255) / 255)
		if len(cluster_points) > 0:
			plot_points(cluster_points, new_figure=False, color=randomColor)

def plot_data(data, **kwargs):
	plot_base(**kwargs)
	# TODO: move in base
	if "label" in kwargs:
		lib_figures[-1].gca().plot(data, label=kwargs["label"])
	else:
		lib_figures[-1].gca().plot(data)

def show_plots(block=True, size=None):
	# TODO: see what params are needed by legend in pyplot
	# lib_figures[-1].legend()
	# plt.legend()
	if not size is None:
		assert len(size) == 2
		figure_set_size(size)
	plt.show(block=block)

def figure_set_size(size):
	lib_figures[-1].set_size_inches(*size)

def figure_clear():
	plt.clf()

def save_figure(name, makeDirs=False):
	# plt.legend()
	if makeDirs:
		directory = os.path.dirname(os.path.abspath(name))
		if not os.path.exists(directory):
			os.makedirs(directory)
	lib_figures[-1].savefig(name)

# metaclasses.py Different metaclass implementations, such as singleton.

# Usage: class SomeClass(metaclass=Singleton)
class Singleton(type):
	classInstances = {}
	def __call__(classType, *args, **kwargs):
		if not classType in classType.classInstances:
			classType.classInstances[classType] = super(Singleton, classType).__call__(*args, **kwargs)
		return classType.classInstances[classType]
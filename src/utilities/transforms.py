# transforms.py Module that implementes regular and homogeneous transforms (translation, rotations in 2D and 3D),
# similar to Peter Corke's Robotics Toolbox.
import numpy as np

# Used for 3D homogeneous transforms
class Homogeneous:
	def __init__(self, matrix = np.eye(4)):
		self.matrix = np.array(matrix)
		assert self.matrix.shape == (4, 4)

	# Override basic multiplication, so we can multiply matrixes together
	def __mul__(self, other):
		return Homogeneous(np.dot(self.matrix, other.matrix))

	def __str__(self):
		return str(self.matrix)

	def __repr__(self):
		return str(self.matrix)

class HTranslation(Homogeneous):
	def __init__(self, x = 0, y = 0, z = 0):
		super().__init__()
		self.matrix[0 : 3, 3] = x, y, z

class HRotationX(Homogeneous):
	def __init__(self, angle):
		super().__init__()
		self.matrix[1 : 3, 1] = np.cos(angle), np.sin(angle)
		self.matrix[1 : 3, 2] = -np.sin(angle), np.cos(angle)

class HRotationY(Homogeneous):
	def __init__(self, angle):
		super().__init__()
		self.matrix[0, 0] = np.cos(angle)
		self.matrix[0, 2] = np.sin(angle)
		self.matrix[2, 0] = -np.sin(angle)
		self.matrix[2, 2] = np.cos(angle)

class HRotationZ(Homogeneous):
	def __init__(self, angle):
		super().__init__()
		self.matrix[0 : 2, 0] = np.cos(angle), np.sin(angle)
		self.matrix[0 : 2, 1] = -np.sin(angle), np.cos(angle)

def getRandomPose():
	return HTranslation(*np.random.normal(size=(3, ))) * HRotationX(*np.random.normal(size=(1, ))) * \
		HRotationY(*np.random.normal(size=(1, ))) * HRotationZ(*np.random.normal(size=(1, )))

# distances.py Module implementing different types of distances regarding points or lines in an Euclidean space
import math
from utilities.constants import *

def euclidean_distance(p1, p2):
	assert len(p1) == len(p2)
	sum = 0
	for i in range(len(p1)):
		sum += (p1[i] - p2[i]) ** 2
	return math.sqrt(sum)

def manhattan_distance(p1, p2):
	assert len(p1) == len(p2)
	sum = 0
	for i in range(len(p1)):
		sum += abs(p1[i] - p2[i])
	return sum	

# Given a line defined by p1 and p2 and a point, compute the distance between them. This is equivalent to the distance
#  of the perpendicular to the line.
def distance_point_line(p1, p2, p):
	top = abs((p2[Y]-p1[Y]) * p[X] - (p2[X]-p1[X]) * p[Y] + p2[X]*p1[Y] - p2[Y]*p1[X])
	bottom = math.sqrt((p2[Y] - p1[Y])**2 + (p2[X] - p1[X])**2)
	return top / bottom

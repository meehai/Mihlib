#interpolations.py Module that implements different types of interpolations of 2 or multiple points in an Euclidean 
# space
import numpy as np
from utilities.constants import *
from .lines import getLineParams_mb, getIntersectionPoint_mb
from .distances import distance_point_line

# Given 2 points, P1 and P2, find the y value of the desired point
# desired_P must be in the interval [P1, P2]
def linear_interpolation(P1, P2, desired_X=None):
	if desired_X is None:
		desired_X = (P1[X] + P2[X]) / 2
	assert (P1[X] <= desired_X and P2[X] >= desired_X) or (P1[X] >= desired_X and P2[X] <= desired_X)

	# Linear interpolation means that the new point (X,Y) will be on the line defined by P1 and P2
	(m, b) = getLineParams_mb(P1, P2)

	# Therefore the m(P1,P2) = m(P1, P) = m(P2, P)
	# m = (y-y_P1)/(x-x_P1) => y-y_P1 = m*(x-x_P1) => y = m*(x-x_P1) + y_P1
	desired_Y = m * (desired_X - P1[X]) + P1[Y]
	return (desired_X, desired_Y)

# Given N sorted points, return the lines that intersect them 2 by 2
def linear_interpolation_points(data, desired_points=None):
	assert(desired_points is None or len(desired_points) == len(data) - 1)
	result = []
	for i in range(len(data) - 1):
		P1 = data[i]
		P2 = data[i+1]
		assert(P1[X] < P2[X])
		result.append(getLineParams_mb(P1, P2))
	return result

def bilinear_interpolation(points, desired_point=None):	
	P1, P2, P3, P4 = points
	# Points must for a rectangle
	assert P1[X] == P4[X] and P2[X] == P3[X] and P1[Y] == P2[Y] and P3[Y] == P4[Y]

	if desired_point is None:
		desired_point = ((P1[X]+P2[X]+P3[X]+P4[X]) / 4, (P1[Y]+P2[Y]+P3[Y]+P4[Y]) / 4)
	# desired points must be inside the rectangle
	assert P1[X] <= desired_point[X] and P1[Y] >= desired_point[Y] and \
		   P2[X] >= desired_point[X] and P2[Y] >= desired_point[Y] and \
		   P3[X] >= desired_point[X] and P3[Y] <= desired_point[Y] and \
		   P4[X] <= desired_point[X] and P4[Y] <= desired_point[Y]

	# Our main function is z=f(x,y) and we know 4 points
	#  P1.............P2
	#  .              .
	#  .              .
	#  .              .
	#  P4.............P3
	# Idea: Generate the 2 line-functions for the constant Ys
	# First find the linear interpolation when Y is constant (so the line will be defined by x and z) (2 points)
	# Then using those 2 found points, find the linear interpolation where x is constant
	# mxz' = (zp'-z1)/(xp'-x1) = (zp'-z2)/(xp'-x2) = (z1-z1)/(x1-x2)
	# mxz'' = Same for (xp'',zp'')
	# Using this, get the line that has constant x that passes through the lines given by mxz' and mxz''
	# on points (zp' and zp'')

	x1, x2, y1, y2 = P1[X], P2[X], P3[Y], P2[Y]
	z1, z2, z3, z4 = P1[Z], P2[Z], P3[Z], P4[Z]
	x3, x4 = x2, x1

	#print("x1=", x1, "x2=", x2, "y1=", y1, "y2=", y2)
	#print("z1=", z1, "z2=", z2, "z3=", z3, "z4=", z4)

	xp, yp, zp = desired_point[X], desired_point[Y], 0
	xp_prim, yp_prim = xp, y2
	xp_second, yp_second = xp, y1

	zp_prim = (z1-z2)/(x1-x2)*(xp_prim-x1) + z1
	zp_second = (z3-z4)/(x3-x4)*(xp_second-x3) + z3
	zp = (zp_prim-zp_second)/(yp-yp_second)*(yp-yp_prim) + zp_prim
	#print("Zp=", zp, "zp'=", zp_prim, "zp''=", zp_second)

	return zp


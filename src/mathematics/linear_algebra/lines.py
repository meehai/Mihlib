# lines.py Module that implements different functions regarding Euclidean lines
from utilities.constants import *
from utilities.others import *

# (x - x1) / (x2 - x1) = (y - y1) / (y2 - y1)
def getLineParams(p1, p2):
	a = p2[Y] - p1[Y]
	b = p1[X] - p2[X]
	c = p1[Y] * (p2[X] - p1[X]) - p1[X] * (p2[Y] - p1[Y])
	return a, b, c

# Given 2 points, get the slope and the y intercept
def getLineParams_mb(p1, p2):
	if p1[X] == p2[X]:
		return (0, p1[Y])
	m = (p1[Y] - p2[Y]) / (p1[X] - p2[X])
	b = p1[Y] - m * p1[X]
	return (m, b)

# Given 2 lines, get the intersection point, unless they're parallel, where None is returned.
# y = m1*x + b1 = m2*x + b2 => (m2-m1)x + (b2-b1) = 0 => x = -(b2-b1)/(m2-m1)
def getIntersectionPoint_mb(l1, l2):
	(m1, b1) = l1
	(m2, b2) = l2
	if m1 == m2:
		return None
	x = -(b2 - b1) / (m2 - m1)
	y = m1 * x + b1
	return (x, y)

# Given 2 lines, get the intersection point, unless they're parallel, where None is returned.
# Robust for vertical/horizontal line problems.
def getIntersectionPoint(l1, l2):
	a1, b1, c1 = l1
	a2, b2, c2 = l2

	# equations are simply equal, or both lines are vertical or horizontal, or they are parallel
	if l1 == l2 or (a1 == 0 and a2 == 0) or (b1 == 0 and b2 == 0) or (a2 != 0 and b2 != 0 and a1 / a2 == b1 / b2):
		return None

	if isVerticalLine(l1):
		if isHorizontalLine(l2):
			return -c1 / a1, -c2 / b2
		# Normal l2 + vertical l1. So we have ax + c = 0 and a'x + b'y + c' = 0.
		# Write the equation as x in both cases: x = eq1 = eq2, and get y from the equality
		else:
			return -c1 / a1, (c1 * a2) / (b2 * a1) - (c2 / b2)
	elif isVerticalLine(l2):
		return getIntersectionPoint(l2, l1)
	# ax+by+c = a'x+b'y+c = 0. Write both equations by x or y, equal them and then find the other based on first.
	else:
		y = -(c2 * a1 - c1 * a2) / (b2 * a1 - b1 * a2)
		x = (-b1 / a1) * y - (c1 / a1)
		return (x, y)

# Returns true if p is on the line given by a,b,c parameters, false otherwise.
def isPointOnLine(line, p):
	a, b, c = line
	return abs(a * p[X] + b * p[Y] + c) < EPS

# Returns true if p is on the segment [p1, p2], false otherwise.
def isPointOnSegment(p1, p2, p):
	line = getLineParams(p1, p2)
	minX, maxX = minMax(p1[X], p2[X])
	minY, maxY = minMax(p1[Y], p2[Y])
	return isPointOnLine(line, p) \
		and (minX <= p[X] or abs(minX - p[X]) <= EPS) and (maxX >= p[X] or abs(maxX - p[X]) <= EPS) \
		and (minY <= p[Y] or abs(minY - p[Y]) <= EPS) and (maxY >= p[Y] or abs(maxY - p[Y]) <= EPS)

def isHorizontalLine(line):
	a, b, c = line
	return a == 0

def isVerticalLine(line):
	a, b, c = line
	return b == 0
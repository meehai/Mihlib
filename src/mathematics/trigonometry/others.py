import numpy as np

def radToDeg(x):
	return x / np.pi * 180

def degToRad(x):
	return x / 180 * np.pi
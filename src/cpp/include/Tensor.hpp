/* Tensor.hpp - Generic class for images and convert operators for various image processing libraries */
#ifndef TENSOR_HPP
#define TENSOR_HPP

#include <algorithm>
#include <array>
#include <cassert>
#include <cstring>
#include <sstream>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <tuple>
#include <type_traits>
#include <typeinfo>

#include <utils.hpp>
#include <PointerWrapper.hpp>
#include <NumpyArray.hpp>

/* Tensor class */
template <typename T, bool OwnsMemory, size_t N>
struct Tensor : public PointerWrapper<T, OwnsMemory> {
	Tensor() = delete;
	/* Allocate using a given std::array for shape */
	Tensor(const std::array<size_t, N> &);
	/* Only allocates the image without setting any value to the allocated memory */
	template <typename ... Args>
	Tensor(Args ...);
	/* Compatibility wih a raw pointer of a given shape */
	Tensor(T *, const std::array<size_t, N> &);
	/* Compatibility with NumpyArray */
	Tensor(NumpyArray *);
	/* Destructor, free the allocated memory */
	~Tensor();

	template <typename T1, bool OwnsMemory1, size_t N1>
	friend std::ostream &operator<<(std::ostream &, const Tensor<T1, OwnsMemory1, N1> &);

	/* Operators for tensor access (h, w, ...). */
	template <typename ... Args>
	const T &operator()(Args ...) const;
	template <typename ... Args>
	T &operator()(Args ...);
	/* Same, but with an array, so we can do t(arr), where arr is an array somewhere defined. */
	const T &operator()(const std::array<size_t, N> &) const;
	T &operator()(const std::array<size_t, N> &);

	/* Returns the indexes[dim] value (which represents the shape on dimension dim). */
	size_t shape(size_t) const;
	const std::array<size_t, N> &shape() const;

	/* Creates a new non-owning tensor that points to the same memory as this, but reshaped properly. */
	template <size_t N1>
	Tensor<T, false, N1> view(const std::array<size_t, N1> &);
	template <size_t N1>
	const Tensor<T, false, N1> view(const std::array<size_t, N1> &) const;
	template <typename ... Args>
	Tensor<T, false, std::tuple_size<std::tuple<Args...>>::value> view(Args ...);
	template <typename ... Args>
	const Tensor<T, false, std::tuple_size<std::tuple<Args...>>::value> view(Args ...) const;

	/* Helper methods for various tensor operations */
	void fill(const T &value);
	void print(const std::array<std::pair<size_t, size_t>, N> &);

private:
	std::array<size_t, N> computeNpArrayIndexes(NumpyArray *);
	/* Variable args -> std::array<size_t, N> */
	template <typename ... Args>
	std::array<size_t, N> computeIndexes(Args ...);
	size_t computeIndexesProd();
	const std::array<size_t, N - 1> computeCumProd();

	const std::array<size_t, N> indexes;
	/* Besides storing the array of indexes, compute helpful values, like the product or the cummulative product */
	const std::array<size_t, N - 1> cumProd;
	const size_t indexesProd;
};

template <typename T, bool OwnsMemory=true>
using Image3D = Tensor<T, OwnsMemory, 3>;

template <typename T>
using Image3D_ = Image3D<T, false>;

template <typename T, bool OwnsMemory=true>
using Image2D = Tensor<T, OwnsMemory, 2>;

template <typename T>
using Image2D_ = Image2D<T, false>;

// /* Used for std::cout/cerr printing */
template <typename T, bool OwnsMemory, size_t N>
std::ostream &operator<<(std::ostream &os, const Tensor<T, OwnsMemory, N> &tensor)
{
	os << "Tensor [" << (OwnsMemory ? "" : "non-") << "owning memory]; ";
	os << "Shape: " << tensor.indexes << "; Prod: " << tensor.indexesProd << "; Cum Prod: " << tensor.cumProd;
	return os;
}

/* Due to ordering problems (Base-class constructor is always called before), we cannot pre-initialize indexes, so we
 *  use the secondary function computerVarArgsProd to compute the product of the Tensor shape for allocation. */
template <typename T, bool OwnsMemory, size_t N>
template <typename ... Args>
Tensor<T, OwnsMemory, N>::Tensor(Args ... args)
	: PointerWrapper<T, OwnsMemory>(computeVarArgProd(args...)), indexes(computeIndexes(args ...)),
	cumProd(computeCumProd()), indexesProd(computeIndexesProd()) {
	static_assert(OwnsMemory == true, "For the case of creating manually a new Tensor, it must also own the memory");
}

template <typename T, bool OwnsMemory, size_t N>
inline
Tensor<T, OwnsMemory, N>::Tensor(const std::array<size_t, N> &array)
	: PointerWrapper<T, OwnsMemory>(computeArrayProd(array)), indexes(array), cumProd(computeCumProd()),
	indexesProd(computeIndexesProd()) {
	static_assert(OwnsMemory == true, "For the case of creating manually a new Tensor, it must also own the memory");
}

template <typename T, bool OwnsMemory, size_t N>
inline
Tensor<T, OwnsMemory, N>::Tensor(T *ptr, const std::array<size_t, N> &shape)
	: PointerWrapper<T, OwnsMemory>(ptr, computeArrayProd(shape)), indexes(shape), cumProd(computeCumProd()),
	indexesProd(computeIndexesProd()) {}

// /* Same here as above, use use the auxilliary function npComputeShapeProd to compute the product of shape. */
template <typename T, bool OwnsMemory, size_t N>
inline
Tensor<T, OwnsMemory, N>::Tensor(NumpyArray *npArray) : Tensor((T *)npArray->data, computeNpArrayIndexes(npArray)) {
	bool typeCheck = (
		(npArray->type == NumpyArrayType::NP_UINT8 && typeid(T) == typeid(uint8_t)) ||
		(npArray->type == NumpyArrayType::NP_INT32 && typeid(T) == typeid(int32_t)) ||
		(npArray->type == NumpyArrayType::NP_INT64 && typeid(T) == typeid(int64_t)) ||
		(npArray->type == NumpyArrayType::NP_FLOAT32 && typeid(T) == typeid(float)) ||
		(npArray->type == NumpyArrayType::NP_FLOAT64 && typeid(T) == typeid(double))
	);

	if(!typeCheck) {
		throw std::runtime_error("NumpyArray is of type: " + npArrayTypeToString(npArray) + "; CPP tensor has type: "
			+ typeid(T).name());
	}
}

// /* We use the cumProd array to compute the values like i * width * depth + j * depth + k * 1];
//  * Shape: (3, 5, 4); Cum prod: (20, 4). So access (i, j, k) = 20 * i + 4 * j + k.
//  * This is equaivalent to a dot product, so we do that. Note that cumProd is an array of size N -1 (last elem is 1)
//  */

template <typename T, bool OwnsMemory, size_t N>
T &Tensor<T, OwnsMemory, N>::operator()(const std::array<size_t, N> &accessIndex) {
	/* Assert so it's disabled in release mode (no bound checks) */
	assert( ([&accessIndex, this]() {
		for(size_t i = 0; i < N; i++) {
			if(this->indexes[i] <= accessIndex[i]) {
				std::cerr << "Out of bounds at index " << i << "; Shape: " << this->shape() << "; Access: "
					<< accessIndex << "\n";
				return false;
			}
		}
		return true;
	})());

	size_t index = std::inner_product(this->cumProd.begin(),
		this->cumProd.end(), accessIndex.begin(), 0);
	index += accessIndex[N - 1];
	return this->basePointer[index];
}

template <typename T, bool OwnsMemory, size_t N>
const T &Tensor<T, OwnsMemory, N>::operator()(const std::array<size_t, N> &accessIndex) const {
	/* Assert so it's disabled in release mode (no bound checks) */
	assert( ([&accessIndex, this]() {
		for(size_t i = 0; i < N; i++) {
			if(this->indexes[i] <= accessIndex[i]) {
				std::cerr << "Out of bounds at index " << i << "; Shape: " << this->shape() << "; Access: "
					<< accessIndex << "\n";
				return false;
			}
		}
		return true;
	})());

	size_t index = std::inner_product(this->cumProd.begin(),
		this->cumProd.end(), accessIndex.begin(), 0);
	index += accessIndex[N - 1];
	return this->basePointer[index];
}

template <typename T, bool OwnsMemory, size_t N>
template <typename ... Args>
T &Tensor<T, OwnsMemory, N>::operator()(Args ... args) {
	std::array<size_t, N> accessIndex = {static_cast<size_t>(args)...};
	return this->operator()(accessIndex);
}

template <typename T, bool OwnsMemory, size_t N>
template <typename ... Args>
const T &Tensor<T, OwnsMemory, N>::operator()(Args ... args) const {
	std::array<size_t, N> accessIndex = {static_cast<size_t>(args)...};
	return this->operator()(accessIndex);
}

template <typename T, bool OwnsMemory, size_t N>
inline
size_t Tensor<T, OwnsMemory, N>::shape(size_t dim) const {
	return this->indexes[dim];
}

template <typename T, bool OwnsMemory, size_t N>
inline
const std::array<size_t, N> &Tensor<T, OwnsMemory, N>::shape() const {
	return this->indexes;
}

template <typename T, bool OwnsMemory, size_t N>
template <size_t N1>
inline
Tensor<T, false, N1> Tensor<T, OwnsMemory, N>::view(const std::array<size_t, N1> &shapeArray) {
	if(computeArrayProd(shapeArray) != this->indexesProd) {
		std::stringstream os;
		os << "Invalid view. Original: " << *this << "; Asked for a shape of: " << shapeArray;
		throw std::runtime_error(os.str());
	}
	return Tensor<T, false, N1>(this->basePointer, shapeArray);
}

template <typename T, bool OwnsMemory, size_t N>
template <size_t N1>
inline
const Tensor<T, false, N1> Tensor<T, OwnsMemory, N>::view(const std::array<size_t, N1> &shapeArray) const {
	if(computeArrayProd(shapeArray) != this->indexesProd) {
		std::stringstream os;
		os << "Invalid view. Original: " << *this << "; Asked for a shape of: " << shapeArray;
		throw std::runtime_error(os.str());
	}
	return Tensor<T, false, N1>(this->basePointer, shapeArray);
}

template <typename T, bool OwnsMemory, size_t N>
template <typename ... Args>
inline
Tensor<T, false, std::tuple_size<std::tuple<Args...>>::value> Tensor<T, OwnsMemory, N>::view(Args ... args) {
	const std::array<size_t, std::tuple_size<std::tuple<Args...>>::value> newShape = {static_cast<size_t>(args) ...};
	return this->view(newShape);
}

template <typename T, bool OwnsMemory, size_t N>
template <typename ... Args>
inline
const Tensor<T, false, std::tuple_size<std::tuple<Args...>>::value> Tensor<T, OwnsMemory, N>::view(Args ...args)
	const {
	const std::array<size_t, std::tuple_size<std::tuple<Args...>>::value> newShape = {static_cast<size_t>(args) ...};
	return this->view(newShape);
}

template <typename T, bool OwnsMemory, size_t N>
inline
void Tensor<T, OwnsMemory, N>::fill(const T &value) {
	std::fill(&this->basePointer[0], &this->basePointer[this->indexesProd], value);
}

template <typename T, bool OwnsMemory, size_t N>
inline
void Tensor<T, OwnsMemory, N>::print(const std::array<std::pair<size_t, size_t>, N> &arr) {
	static_assert (N == 3);
	assert(arr.size() == 3);

	std::cerr << "[\n";
	for(size_t i = arr[0].first; i < arr[0].second; i++) {
		std::cerr << "  [\n";
		for(size_t j = arr[1].first; j < arr[1].second; j++) {
			std::cerr <<"    [ ";
			for(size_t k = arr[2].first; k < arr[2].second; k++) {
				std::cerr << this->operator()(i, j, k) << " ";
			}
			std::cerr << "]\n";
		}
		std::cerr << "  ]\n";
	}
	std::cerr << " ]\n";
}

template <typename T, bool OwnsMemory, size_t N>
inline
std::array<size_t, N> Tensor<T, OwnsMemory, N>::computeNpArrayIndexes(NumpyArray *npArray) {
	if(npArray->shapeSize > N) {
		throw std::runtime_error("Received an array with a shape larger than the given type. Given array dimensions " +
			std::to_string(npArray->shapeSize) + "; CPP dimensions " + std::to_string(N));
	}

	std::array<size_t, N> indexesArr;
	/* Initially, copy the shape from the npArray->shape pointer */
	for(size_t i = 0; i < npArray->shapeSize; i++) {
		indexesArr[i] = npArray->shape[i];
	}

	/* Pad with 1 the rest of the shapes, if needed (i.e. using a Image3D for a 2D array => HxWx1 shape) */
	for(size_t i = npArray->shapeSize; i < N; i++) {
		indexesArr[i] = 1;
	}

	return indexesArr;
}

template <typename T, bool OwnsMemory, size_t N>
template <typename ... Args>
inline
std::array<size_t, N> Tensor<T, OwnsMemory, N>::computeIndexes(Args ... args) {
	std::array<size_t, N> indexesArr = {static_cast<size_t>(args) ...};
	const size_t NArgs = std::tuple_size<std::tuple<Args...>>::value;
	/* If we receive less arguments than what the type uses (i.e using 2D array for Image3D), fill with 1 the rest of
	 *  the dimensions. */
	std::fill(&indexesArr[NArgs], &indexesArr[N], 1);
	return indexesArr;
}

template <typename T, bool OwnsMemory, size_t N>
const std::array<size_t, N - 1> Tensor<T, OwnsMemory, N>::computeCumProd() {
	std::array<size_t, N - 1> cumProd;
	size_t prod = 1;
	for(size_t i = N - 1; i >= 1; i--) {
		prod *= this->indexes[i];
		cumProd[i - 1] = prod;
	}
	return cumProd;
}

template <typename T, bool OwnsMemory, size_t N>
size_t Tensor<T, OwnsMemory, N>::computeIndexesProd() {
	return std::accumulate(this->indexes.begin(), this->indexes.end(), 1, std::multiplies<size_t>());
}

template <typename T, bool OwnsMemory, size_t N>
inline
Tensor<T, OwnsMemory, N>::~Tensor() {}

#endif /* TENSOR_HPP */
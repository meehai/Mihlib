#ifndef NUMPY_ARRAY_HPP
#define NUMPY_ARRAY_HPP

#include <cstdint>
#include <iostream>

enum NumpyArrayType {
	NP_UINT8 = 0,
	NP_INT32,
	NP_INT64,
	NP_FLOAT32,
	NP_FLOAT64
};

// type is handled by Python side by calling correct function. ELSE BUGS.
struct NumpyArray {
	char *data;
	size_t *shape;
	size_t shapeSize;
	enum NumpyArrayType type;
};

std::size_t npArraySizeof(const NumpyArray *npArray) {
	switch (npArray->type) {
		case NP_UINT8:
			return sizeof(std::uint8_t);
		case NP_INT32:
			return sizeof(std::int32_t);
		case NP_INT64:
			return sizeof(std::int64_t);
		case NP_FLOAT32:
			return sizeof(float);
		case NP_FLOAT64:
			return sizeof(double);
		default:
			std::cerr << "Only uint8, int32, int64, float32  and float64 are supported.\n";
			std::terminate();
	}
}

std::string npArrayTypeToString(const NumpyArray *npArray) {
	switch (npArray->type) {
		case NP_UINT8:
			return "uint8";
		case NP_INT32:
			return "uint32";
		case NP_INT64:
			return "uint64";
		case NP_FLOAT32:
			return "float32";
		case NP_FLOAT64:
			return "float64";
	}
	return "unknown";
}

#endif /* NUMPY_ARRAY_HPP */
#include <type_traits>
#include <tuple>
#include <NumpyArray.hpp>

#ifndef UTILS_HPP
#define UTILS_HPP

template<std::size_t>
struct DummyCount{};

/* Starting from last one, recurse till first, and then print */
template <typename Tuple, size_t index>
void tuple_printer(std::ostream &os, Tuple tuple, DummyCount<index>) {
	tuple_printer(os, tuple, DummyCount<index - 1>{});
	os << ", " << std::get<index>(tuple);
}

template <typename Tuple>
void tuple_printer(std::ostream &os, Tuple tuple, DummyCount<0>) {
	os << std::get<0>(tuple);
}

template <typename ... Args>
std::ostream &operator<<(std::ostream &os, const std::tuple<Args ...> &tuple) {
	const size_t size = std::tuple_size<std::tuple<Args...>>::value;
	os << "(";
	tuple_printer(os, tuple, DummyCount<size - 1>{});
	os << ")";
	return os;
}

template <typename T, typename U>
std::ostream &operator<<(std::ostream &os, const std::pair<T, U> &pair) {
	os << "(" << pair.first << ", " << pair.second << ")";
	return os;
}

template <typename T, size_t N>
std::ostream &operator<<(std::ostream &os, const std::array<T, N> &array) {
	os << "(";
	for(size_t i = 0; i < N - 1; i++)
		os << array[i] << ", ";
	os << array[N - 1] << ")";
	return os;
}

template <typename T, size_t N>
size_t computeArrayProd(const std::array<T, N> &arr) {
	return std::accumulate(arr.begin(), arr.end(), 1, std::multiplies<size_t>());
}

/* This assumes values are unsigned ints !! */
template <typename ... Args>
size_t computeVarArgProd(Args ... args) {
	const size_t N = std::tuple_size<std::tuple<Args...>>::value;
	std::array<size_t, N> arr = {static_cast<size_t>(args)...};
	return computeArrayProd(arr);
}

size_t npComputeShapeProd(const NumpyArray *npArray) {
	size_t shapeProd = 1, shapeSize = npArray->shapeSize;
	for(size_t i = 0; i < shapeSize; i++) {
		shapeProd *= npArray->shape[i];
	}
	return shapeProd;
}

#endif /* UTILS_HPP */
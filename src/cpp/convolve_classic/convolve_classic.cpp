#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <Tensor.hpp>

extern "C" {
int call_convolve_classic(struct NumpyArray *image, struct NumpyArray *filter, struct NumpyArray *outImage) {
	assert(image->shapeSize == 3 && filter->shapeSize == 2 && outImage->shapeSize == 3);
	assert(image->shape[0] >= filter->shape[0] && image->shape[1] >= filter->shape[1]);
	Image3D_<double> iImage (image);
	Image2D_<double> iFilter (filter);
	Image3D_<double> iOutImage (outImage);

	size_t halfFilterHeight = iFilter.shape(0) / 2;
	size_t halfFilterWidth = iFilter.shape(1) / 2;

	/* Avoid parts where not all kernel can be used for convolution. */
	for(size_t i = halfFilterHeight; i < iImage.shape(0) - halfFilterHeight; ++i) {
		for(size_t j = halfFilterWidth; j < iImage.shape(1) - halfFilterWidth; ++j) {
			for(size_t k = 0; k < iImage.shape(2); ++k) {
				double sum = 0;
				for(size_t filterI = 0; filterI < iFilter.shape(0); ++filterI) {
					for(size_t filterJ = 0; filterJ < iFilter.shape(1); ++filterJ) {
						sum += iImage(i + filterI - halfFilterHeight, j + filterJ - halfFilterWidth, k) *
							iFilter(filterI, filterJ);
					}
				}
				iOutImage(i, j, k) = sum;
			}
		}
	}

	/* For the first rows/last rows, we need a separate loop. */
	for(size_t i = 0; i < halfFilterHeight; i++) {
		for(size_t j = 0; j < iImage.shape(1); j++) {
			for(size_t k = 0; k < iImage.shape(2); k++) {
				iOutImage(i, j, k) = iImage(i, j, k);
				iOutImage(iImage.shape(0) - i - 1, j, k) = iImage(iImage.shape(0) - i - 1, j, k);
			}
		}
	}

	/* For the first column/last colums, also, but the corners are already made in previous loop, so avoid them too. */
	for(size_t i = halfFilterHeight; i < iImage.shape(0) - halfFilterHeight; i++) {
		for(size_t j = 0; j < halfFilterWidth; j++) {
			for(size_t k = 0; k < iImage.shape(2); k++) {
				iOutImage(i, j, k) = iImage(i, j, k);
				iOutImage(i, iImage.shape(1) - j - 1, k) = iImage(i, iImage.shape(1) - j - 1, k);
			}
		}
	}

	return 0;
}
}
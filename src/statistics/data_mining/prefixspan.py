from Mihlib import *
from copy import deepcopy

## Events algorithms
def event_toString(event):
	str = "["
	for i in range(len(event) - 1):
		item = event[i]
		str += item + ", "
	str += event[len(event) - 1] + "]"
	return str

def printEvent(event):
	print(event_toString(event))

# Given 2 events return True if e2 is the prefix of e1, False otherwise
# Examples:
# e1=["A", "B", "C"] and e2=["A", "B"] - True
# e1=["A", "B", "C"] and e2=["A", "C"] - False
def isEventPrefix(e1, e2):
	assert(len(e1) > 0 and len(e2) > 0)

	for i in range(len(e1)):
		item_e1 = e1[i]
		item_e2 = e2[i]
		# If any item of e2 differs from e1, it means it's not a prefix of e1
		if item_e1 != item_e2:
			return False
		# If we reached end of e2, means it is smaller than e1 and thus it is a prefix of e1.
		if i == len(e2) - 1:
			return True
	return False

# Given 2 events e1 and e2, return True if e2 is a subevent of e1, False otherwise
# Examples:
# e1=["A", "B", "C"] and e2=["A", "B"] - True
# e1=["A", "B", "C"] and e2=["A", "C"] - True
# e1=["A", "B", "C"] and e2=["B", "A"] - True
# e1=["A", "B", "C"] and e2=["A", "D"] - False
def isSubEvent(e1, e2):
	assert(len(e1) > 0 and len(e2) > 0)

	if e1 == e2:
		return True

	if "_" in e2:
		count = 0
		while e2[count] == "_":
			count += 1

		for item in e2[count:]:
			if not item in e1:
				return False

		# Find the position of first item in e2 that is after _ and see that it's after enough _ to fit
		if count > e1.index(e2[count]):
			return False
		return True
	elif "_" in e1:
		#print("Here", e1, e2)
		count = 0
		while e1[count] == "_":
			count += 1

		atLeastOne = False
		for item in e2:
			if not item in e1[count:]:
				if count == 0:
					return False
				else:
					count -= 1
			else:
				atLeastOne = True

		if count == 0:
			# This avoids [_, d] and [a], but keeps [_, d] [a, d]
			if atLeastOne:
				return True
			else:
				return False
		else:
			return False
	else:
		for item in e2:
			if not item in e1:
				return False
	return True

# Given two events e1 and e2, compute e1-e2 
def getDifferenceEvents(event1, event2):
	diff_event = []
	for item in event1:
		if item not in event2:
			diff_event.append(item)
	return diff_event

## Sequence of events algorithms
def sequence_toString(sequence):
	str = "<"
	for i in range(len(sequence) - 1):
		event = sequence[i]
		str += event_toString(event) + ", "
	str += event_toString(sequence[len(sequence) - 1]) + ">"
	return str

def printSequence(sequence):
	print(sequence_toString(sequence))

def sequenceDatabase_toString(sequences):
	Str = ""
	for key in sequences:
		Str += str(key) + " => " + sequence_toString(sequences[key]) + "\n"
	return Str

def printSequenceDatabase(sequences):
	print(sequenceDatabase_toString(sequences), end="")

# Given a sequence of events, return its size.
# Input: S=<AB, ACE, ANZ,D>
# Return: Size (9)
def getSequenceSize(sequence):
	size = 0
	for event in sequence:
		size += len(event)
	return size

# Given a sequence of events, return the number of events.
def getSequenceLength(sequence):
	return len(sequence)

# Given 2 sequences of events, A and B, return true if B is a prefix of A
# Example: A=<a,abc,ac,d,cf> and B=<a,abc,a> but not C=<a,abc,c>
def isSequencePrefix(A, B):
	for i in range(len(A) - len(B) + 1):
		if i >= len(B):
			return True
		event_A = A[i]
		event_B = B[i]

		if event_A == event_B:
			continue
		else:
			return isEventPrefix(event_A, event_B)

	if i == 0 and len(A) == len(B):
		return isEventPrefix(A[0], B[0])
	return False

# Given 2 sequences of events, return True if seq2 is a subsequence of seq1, False otherwise.
# Examples:
# S1=<ABC, M, BDZ>, S2=<A, DZ> - True
# S1=<ABC, M, BDZ>, S2=<C, M, BZ> - True
# S1=<ABC, M, BDZ>, S2=<C, BZ, M> - False
# S1=<ABC, M, BDZ>, S2=<ABCD, M, DZ> - False
# S1=<ABC, M, BDZ>, S2=<ABC, BZ> - True
def isSubSequence(seq1, seq2):
	i_seq1 = 0

	# For each event K that we search in seq2, we need to find if there is any correspondent in seq1, and at what index
	# the first one appears. The next event K+1 in seq2 will be searched starting from this index only.
	for event_seq2 in seq2:
		# Find the subEvent of event_seq2 in seq1
		found = False
		while i_seq1 < len(seq1):
			event_seq1 = seq1[i_seq1]
			if isSubEvent(event_seq1, event_seq2):
				found = True
				break
			i_seq1 += 1
		if not found:
			return False
		i_seq1 += 1
	return True

# Given a sequence database and a sequence, compute the support of the sequence in the database
def getSequenceSupport(sequences, checkedSequence):
	support = 0
	for key in sequences:
		sequence = sequences[key]
		if isSubSequence(sequence, checkedSequence):
			support += 1
	return support

# ["A", "B", "C"] and ["B"] will return ["C"]
# ["A", "B", "C", "D"] and ["A", "B"] will return ["C", "D"]
def getFollowingEvent(event1, event2):
	new_event = []
	for i_event1 in range(len(event1) - len(event2) + 1):
		i = i_event1
		j = 0
		while j < len(event2) and i < len(event1) and (event1[i] == event2[j] or event2[j] == "_"):
			i += 1
			j += 1
		# Found our following sequence
		if j == len(event2):
			return event1[i_event1+len(event2):]
	return []

# Given 2 sequences of events, seq1 and seq2, return the projection of seq1 for which seq2 is its prefix.
# Example: seq1=<A,ABC,AC,D,CF> and seq2=<BC,A> => seq'=<BC,AC,D,CF>
def getSequenceProjection(seq1, seq2):
	assert(isSubSequence(seq1, seq2))
	# This is the loop from isSubSequence, we need to do it again to get the last index, where the projection will 
	# start.
	i_seq1 = 0
	i_seq2 = 0
	projection_seq = []
	for i_seq2 in range(len(seq2)):
		# Avoid copying using list()
		event_seq2 = deepcopy(seq2[i_seq2])
		while i_seq1 < len(seq1):
			event_seq1 = seq1[i_seq1]
			if isSubEvent(event_seq1, event_seq2):
				break
			i_seq1 += 1

		projection_seq.append(event_seq2)
		i_seq1 += 1
		i_seq2 += 1
	
	# The last one might also contain additional things from seq1
	following = getFollowingEvent(seq1[i_seq1-1], seq2[i_seq2-1])
	for item in following:
		projection_seq[len(projection_seq)-1].append(item)

	# The first events in the projection is the differnece between the two last events checked.
	while i_seq1 < len(seq1):
		projection_seq.append(seq1[i_seq1])
		i_seq1 += 1
	assert(isSequencePrefix(projection_seq, seq2))
	return projection_seq

# Given 2 sequences of events, seq1 and seq2, return the postfix of the events, which is the difference between the
# (projection of seq1 and seq2) and seq2
# Example: seq1=<A,ABC,AC,D,CF> and seq2=<BC,A> => seq'=<BC,AC,D,CF> => post=<_C, D, CF>
def getSequencePostfix(seq1, seq2):
	assert(isSubSequence(seq1, seq2))
	# This is the loop from isSubSequence, we need to do it again to get the last index, where the projection will 
	projection_seq = getSequenceProjection(seq1, seq2)
	if projection_seq == []:
		return []

	for i_seq2 in range(len(seq2)):
		if projection_seq[0] == seq2[i_seq2]:
			projection_seq.pop(0)
		else:
			value = projection_seq.pop(0)
			res = getFollowingEvent(value, seq2[i_seq2])
			for i in range(len(seq2[i_seq2])):
				res.insert(0, "_")
			projection_seq.insert(0, res)
			break
	return projection_seq

def createNewDataSet(dataSet, sequence):
	newDataSet = {}
	for key in dataSet:
		dataSetSequence = dataSet[key]
		if isSubSequence(dataSetSequence, sequence):
			projection = getSequencePostfix(dataSetSequence, sequence)
			if projection != []:
				newDataSet[key] = projection
	assert(newDataSet != [])
	return newDataSet

# dataSet is the original dataset for step 0 and the projected dataSet for step > 0
# recursively generate all solutions and extend the list of previous step
# return all the frequent sequences
def prefixSpan(dataSet, minSup, items, currentSeq=[], depth=0):
	result = []

	if len(dataSet) == 1:
		return []

	for item in items:
		sequence = [[item]]
		support = getSequenceSupport(dataSet, sequence)
		if support >= minSup:
			newDataSet = createNewDataSet(dataSet, sequence)
			newSeq = list(currentSeq)
			newSeq.append([item])
			result.append(newSeq)
			result.extend(prefixSpan(newDataSet, minSup, items, newSeq, depth+1))

		if len(currentSeq) > 0:
			sequence = [["_", item]]
			support = getSequenceSupport(dataSet, sequence)
			if support >= minSup:
				pass
				newDataSet = createNewDataSet(dataSet, sequence)
				newSeq = deepcopy(currentSeq)
				if not item in newSeq[len(newSeq)-1]:
					newSeq[len(newSeq)-1].extend(item)
					result.append(newSeq)
					result.extend(prefixSpan(newDataSet, minSup, items, newSeq, depth+1))
	return result
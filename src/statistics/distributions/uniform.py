from .distribution import DiscreteDistribution
import numpy as np

# Associate the uniform distribution with “rolling a fair die.”
class DiscreteUniform(DiscreteDistribution):
	def __init__(self, values):
		self.values = values

	def sample(self):
		# sample from the uniform sampler
		sample = np.random.uniform(0, 1)
		# split the values in identical intervals in [0, 1] and find the first interval where the value fits
		cumSum = np.cumsum(np.ones(len(self.values)) / len(self.values))
		where = np.where(cumSum >= sample)[0][0]
		return self.values[where]
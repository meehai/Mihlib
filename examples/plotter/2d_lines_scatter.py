from Mihlib import *
import numpy as np

def main():
	func = lambda x : x ** 2 + 2 * x + 1
	x = np.array(flrange(-10, 10, 1))
	y = func(x)

	fig = Figure((2, 1))
	fig.at(0, 0).plot_data((x, y), label="Hello", color="pink")
	fig.at(1, 0).plot_data((x, y), label="Hello2", marker="x")
	fig.at(1, 0).plot_data((x*2, y), label="Hello3", line_interpolation=True)
	fig.plot()
	show_plots()

if __name__ == "__main__":
	main()
import sys
from wrappers.pytorch.neural_network import NeuralNetworkPyTorch, maybeCuda
from callbacks import SaveHistory, SaveModels
from utilities.public_api import Cifar10Reader

import torch as tr
import torch.nn as nn
import torch.nn.functional as F

class Cifar10Model(NeuralNetworkPyTorch):
	def __init__(self):
		super().__init__()
		self.conv1 = nn.Conv2d(in_channels=3, out_channels=96, kernel_size=3, stride=1)
		self.fc1 = nn.Linear(96 * 15 * 15, 100)
		self.fc2 = nn.Linear(100, 10)
		self.pool22 = nn.MaxPool2d(2, 2)

	def forward(self, x):
		x = self.pool22(F.relu(self.conv1(x)))
		x = x.view(-1, 96 * 15 * 15)
		x = F.relu(self.fc1(x))
		x = self.fc2(x)
		x = F.log_softmax(x)
		return x

if __name__ == "__main__":
	assert len(sys.argv) == 2
	reader = Cifar10Reader(sys.argv[1], transforms=["standardize"])
	data, labels = reader.getData("train")
	val_data, val_labels = reader.getData("validation")

	model = maybeCuda(Cifar10Model())
	model.setOptimizer(tr.optim.SGD, lr=0.01, momentum=0.9)
	model.setCriterion(F.nll_loss)
	model.setMetrics(["Accuracy"])
	# model.summary()
	callbacks = [SaveHistory("history.txt"), SaveModels(type="improvements")]
	model.train(data, labels, batchSize=100, numEpochs=10, callbacks=callbacks,\
		validationData=val_data, validationLabels=val_labels)
from Mihlib import *
import numpy as np
import sys

def ex_linear_interpolation_2points():
	P1, P2 = (0, 0), (0, 0)
	while abs(P1[X] - P2[X]) < 5:
		[P1, P2] = generateRandomPoints(2)
	P3 = linear_interpolation(P1, P2, (P1[X] + P2[X]) / 2)
	print("Linear interpolation of 2 points:", P1, P2, "Result:", P3)
	plot_points( np.array([P1, P2, P3]), new_figure=True, Range=[minMax(P1[X], P2[X])], title="Linear interpolation" )
	plot_line(getLineParams_mb(P1, P2), Range=minMax(P1[X], P2[X]), new_figure=False)

def ex_linear_interpolation_Npoints(N):
	data = []
	current_X = []
	# Generate random points that have unique f(x) = y
	for i in range(N):
		while True:
			point = (randint(0, 99), randint(0, 99))
			if not point[X] in current_X:
				current_X.append(point[X])
				data.append(point)
				break
	print("Linear interpolation of N=", N, "points:", data)
	data = sorted(data)

	plot_points(np.array(data), new_figure=True, title="Linear interpolation N points")
	lines = linear_interpolation_points(data)
	for i in range(len(lines)):
		line = lines[i]
		P1 = data[i]
		P2 = data[i+1]
		plot_line(line, Range=[min(P1[X], P2[X]), max(P1[X], P2[X])], new_figure=False)

def main():
	assert len(sys.argv) == 2, "Desired number of points to interpolate linearly"

	ex_linear_interpolation_2points()
	print("-------------")

	N = int(sys.argv[1])
	ex_linear_interpolation_Npoints(N)
	print("-------------")

	show_plots(block=True)

if __name__ == "__main__":
	main()

from Mihlib import *
from sys import argv
from zipfile import ZipFile

def getArchive():
	archive_url = "http://www.uni-marburg.de/fb12/datenbionik/downloads/FCPS"
	local_archive = "FCPS.zip"
	from os import path
	if not path.isfile(local_archive):
		import urllib.request
		print("downloading...")
		urllib.request.urlretrieve(archive_url, filename=local_archive)
		assert(path.isfile(local_archive))
		print("got the archive")
	return ZipFile(local_archive)

def getDataSet(archive, dataSetName):
	path = "FCPS/01FCPSdata/" + dataSetName

	lrnFile = path + ".lrn"
	with archive.open(lrnFile, "r") as f:                       # open .lrn file
		N = int(f.readline().decode("UTF-8").split()[1])    # number of examples
		D = int(f.readline().decode("UTF-8").split()[1]) - 1 # number of columns
		f.readline()                                     # skip the useless line
		f.readline()                                       # skip columns' names
		Xs = []
		for i in range(N):
			data = f.readline().decode("UTF-8").strip().split("\t")
			assert(len(data) == (D+1))                              # check line
			assert(int(data[0]) == (i + 1))
			Xs.append(list(map(float, data[1:])))

	clsFile = path + ".cls"
	with archive.open(clsFile, "r") as f:                        # open.cls file
		labels = [ 0 for _ in range(N) ]

		line = f.readline().decode("UTF-8")
		while line.startswith("%"):                                # skip header
			line = f.readline().decode("UTF-8")

		i = 0
		while line and i < N:
			data = line.strip().split("\t")
			assert(len(data) == 2)
			assert(int(data[0]) == (i + 1))
			labels[i] = int(data[1])
			line = f.readline().decode("UTF-8")
			i = i + 1

		assert(i == N)

	return np.array(Xs), labels                          # return data and correct classes

# 
def randIndex(clusters, labels, K):
	a, b, c, d = 0, 0, 0, 0
	for i in range(len(clusters) - 1):
		point1 = clusters[i]
		truth_point1 = labels[i]
		for j in range(i+1, len(clusters)):
			point2 = clusters[j]
			truth_point2 = labels[j]

			if point1 == point2:
				if truth_point1 == truth_point2:
					a += 1
				else:
					b += 1
			else:
				if truth_point1 == truth_point2:
					d += 1
				else:
					c += 1
	return (a+b) / (a+b+c+d)

def main():
	if len(argv) < 3:
		print("Usage: " + argv[0] + " dataset_name K")
		exit()
	K = int(argv[2])

	data, labels = getDataSet(getArchive(), argv[1])
	clusters, centroids = kMeans_points(data, K, percentChange=0)
	plot_clusters(data, clusters, centroids)

	print("Rand index:", randIndex(clusters, labels, K))
	show_plots()

if __name__ == "__main__":
	main()
from Mihlib import *
from machine_learning.supervised_learning.neural_networks.layers.convolutional_6for import ConvolutionalLayer
import numpy as np

def test_ConvolutionalLayer_forward_1():
	input = [
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 2, 0, 2, 0, 0],
			[0, 2, 0, 2, 0, 0, 0],
			[0, 1, 0, 2, 2, 0, 0],
			[0, 2, 2, 0, 0, 2, 0],
			[0, 2, 1, 2, 2, 1, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 2, 2, 1, 0, 0],
			[0, 0, 1, 1, 2, 1, 0],
			[0, 2, 2, 1, 0, 0, 0],
			[0, 1, 0, 0, 0, 0, 0],
			[0, 2, 2, 2, 2, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 0, 2, 1, 0, 0],
			[0, 0, 2, 0, 0, 0, 0],
			[0, 0, 1, 0, 1, 0, 0],
			[0, 0, 1, 1, 0, 2, 0],
			[0, 1, 2, 0, 2, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
	]

	weights = [
		[
			[
				[0, 1, 0],
				[-1, 0, 0],
				[1, 0, 1]
			],
			[
				[1, -1, 1],
				[0, 0, 0],
				[-1, -1, 1]
			],
			[
				[1, 0, 0],
				[1, -1, 0],
				[-1, 0, 1]
			]
		],
		[
			[
				[-1, 0, 1],
				[-1, 1, 0],
				[0, 0, -1]
			],
			[
				[0, 1, -1],
				[0, 0, 0],
				[0, -1, 0]
			],
			[
				[0, -1, -1],
				[-1, 1, -1],
				[1, -1, -1]
			]
		]
	]

	biases = np.zeros((2, 1, 1, 1))
	biases[0, 0, 0, 0] = 1
	biases[1, 0, 0, 0] = 0

	output = [
		[
			[2, -5, -3],
			[6, 9, 1],
			[1, 3, 3]
		],
		[
			[2, 0, -4],
			[-7, -1, -4],
			[3, -6, -5]
		]
	]

	input = np.expand_dims(np.array(input), axis=0)
	weights = np.array(weights)
	output = np.array(output)
	a = ConvolutionalLayer((3, 7, 7), kernelSize=3, stride=2, numKernels=2)
	a.weights = weights
	a.biases = biases
	res = a.forward(input)
	return close_enough(res, output)

def test_ConvolutionalLayer_forward_2():
	l = ConvolutionalLayer((1, 5, 5), kernelSize=3, stride=1, numKernels=2)

	input = np.array([
		[
			[3, 2, 1, 5, 2],
			[3, 0, 2, 0, 1],
			[0, 6, 1, 1, 10],
			[3, -1, 2, 9, 0],
			[1, 2, 1, 5, 5]
		]
	])

	weights = np.array([
		[
			[
				[0.3, 0.1, 0.2],
				[0.0, -0.1, -0.1],
				[0.05, -0.2, 0.05]
			]
		],
		[
			[
				[0.0, -0.1, 0.1],
				[0.1, -0.2, 0.3],
				[0.2, -0.3, 0.2]
			]
		]
	])

	expected_result = np.array([
		[
			[-0.05, 1.65, 1.45],
			[1.05, 0.00, -2.0],
			[0.40, 1.15, 0.80]
		],
		[
			[-0.8, 1.1, 2.1],
			[0.6, 1.5, 0.7],
			[0.4, 3.3, -1.0]
		]
	])

	l.weights = weights
	l.biases = np.zeros((2,1))
	input = np.expand_dims(input, axis=0)
	result = l.forward(input)

	return close_enough(result, expected_result)

def test_ConvolutionalLayer_backward_1(): 
	input = np.array(
	[
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 2, 0, 2, 0, 0],
			[0, 2, 0, 2, 0, 0, 0],
			[0, 1, 0, 2, 2, 0, 0],
			[0, 2, 2, 0, 0, 2, 0],
			[0, 2, 1, 2, 2, 1, 0],
			[0, 0, 0, 0, 0, 0, 0],
		]
	])

	weights = np.array(
	[
		[
			[
				[0, 1, 0],
				[-1, 0, 0],
				[1, 0, 1]
			]
		]
	])

	biases = np.zeros((1, 1, 1, 1))

	expected_backward_result = np.array(
	[
		[
			[0.0, 1.0, 0.0, -2.0, 0.0, -2.0, 0.0],
			[-1.0, 0.0, 2.0, 0.0, 2.0, 0.0, 0.0],
			[1.0, 4.0, -1.0, 4.0, -4.0, -2.0, -2.0],
			[-4.0, 0.0, -4.0, 0.0, 2.0, 0.0, 0.0],
			[4.0, 2.0, 8.0, -1.0, 2.0, 0.0, -2.0],
			[-2.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
			[2.0, 0.0, 1.0, 0.0, -1.0, 0.0, 0.0]
		]
	])

	expected_g_weights = np.array(
	[
		[
			[
				[-2.0, 20.0, 4.0],
				[-13.0, 16.0, 6.0],
				[ 8.0, 2.0, 8.0]
			]
		]
	])

	expected_g_biases = np.array(
	[
		[4.0]
	])

	l = ConvolutionalLayer((1, 7, 7), kernelSize=3, stride=2, numKernels=1)
	l.weights = weights
	l.biases = biases

	input = np.expand_dims(input, axis=0)
	forward_result = l.forward(input)
	error = forward_result
	error[0, 0, 0, 0] = 1

	backward_result = l.backward(input, error)
	new_weights = l.weights
	new_biases = l.biases
	g_weights = l.gradientWeights
	g_biases = l.gradientBiases

	return close_enough(weights, new_weights) and close_enough(biases, new_biases) and \
		close_enough(g_weights, expected_g_weights) and close_enough(g_biases, expected_g_biases) and \
		close_enough(backward_result, expected_backward_result)

def test_ConvolutionalLayer_backard_2():
	input = np.array(
	[
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 2, 0, 2, 0, 0],
			[0, 2, 0, 2, 0, 0, 0],
			[0, 1, 0, 2, 2, 0, 0],
			[0, 2, 2, 0, 0, 2, 0],
			[0, 2, 1, 2, 2, 1, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 2, 2, 1, 0, 0],
			[0, 0, 1, 1, 2, 1, 0],
			[0, 2, 2, 1, 0, 0, 0],
			[0, 1, 0, 0, 0, 0, 0],
			[0, 2, 2, 2, 2, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
		[
			[0, 0, 0, 0, 0, 0, 0],
			[0, 2, 0, 2, 1, 0, 0],
			[0, 0, 2, 0, 0, 0, 0],
			[0, 0, 1, 0, 1, 0, 0],
			[0, 0, 1, 1, 0, 2, 0],
			[0, 1, 2, 0, 2, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
		],
	])

	weights = np.array(
	[
		[
			[
				[0, 1, 0],
				[-1, 0, 0],
				[1, 0, 1],
			],
			[
				[1, -1, 1],
				[0, 0, 0],
				[-1, -1, 1],
			],
			[
				[1, 0, 0],
				[1, -1, 0],
				[-1, 0, 1],
			],
		],
		[
			[
				[-1, 0, 1],
				[-1, 1, 0],
				[0, 0, -1],
			],
			[
				[0, 1, -1],
				[0, 0, 0],
				[0, -1, 0],
			],
			[
				[0, -1, -1],
				[-1, 1, -1],
				[1, -1, -1],
			],
		],
	])

	biases = np.zeros((2, 1, 1, 1))
	biases[0 ,0, 0, 0] = 1
	biases[1, 0, 0, 0] = 0

	expected_backward_result = np.array(
	[
		[
			[ -1.0, 1.0, 1.0, -5.0, 4.0, -3.0, -4.0],
			[ -2.0, 1.0, 5.0, 0.0, 7.0, -4.0, 0.0],
			[8.0, 6.0, -11.0, 9.0, -5.0, 1.0, -3.0],
			[1.0, -7.0, -8.0, -1.0, 3.0, -4.0, 0.0],
			[3.0, 1.0, 31.0, 3.0, 10.0, 3.0, 0.0],
			[ -4.0, 3.0, 3.0, -6.0, 2.0, -5.0, 0.0],
			[1.0, 0.0, 1.0, 0.0, 12.0, 0.0, 8.0]
		],
		[
			[1.0, 0.0, -5.0, 5.0, -8.0, -1.0, 1.0],
			[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
			[5.0, -15.0, 28.0, -5.0, 9.0, 2.0, 2.0],
			[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
			[ -5.0, 3.0, -2.0, -17.0, 20.0, -5.0, 9.0],
			[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
			[ -1.0, -4.0, -2.0, 3.0, 0.0, 2.0, 3.0]
		],
		[
			[1.0, -1.0, -6.0, 0.0, -3.0, 4.0, 4.0],
			[0.0, 0.0, -6.0, 5.0, 1.0, -1.0, 4.0],
			[6.0, 6.0, 21.0, 1.0, -4.0, 8.0, 5.0],
			[ 13.0, -13.0, 17.0, -10.0, 6.0, -5.0, 4.0],
			[-12.0, 4.0, 3.0, 7.0, 14.0, 9.0, 10.0],
			[ -2.0, 2.0, 6.0, -9.0, 14.0, -8.0, 5.0],
			[2.0, -3.0, -11.0, 6.0, 1.0, 5.0, 8.0]
		],
	])

	expected_g_weights = np.array(
	[
		[
			[
				[ 6.0, 38.0, 2.0],
				[ -5.0, 37.0, 17.0],
				[ 18.0, 6.0, 12.0]
			],
			[
				[ 11.0, 11.0, 24.0],
				[ 17.0, 19.0, 17.0],
				[-11.0, -2.0, -9.0]
			],
			[
				[ 21.0, 9.0, 13.0],
				[ 19.0, -7.0, 18.0],
				[ -1.0, 11.0, 8.0]
			]
		],
		[
			[
				[-12.0, -20.0, 6.0],
				[-32.0, -18.0, -9.0],
				[ -2.0, -20.0, -14.0]
			],
			[
				[ -9.0, -2.0, -9.0],
				[-28.0, -21.0, -18.0],
				[ -8.0, -11.0, 1.0]
			],
			[
				[ -8.0, -16.0, -11.0],
				[-31.0, 5.0, -14.0],
				[ -1.0, -9.0, -5.0]
			]
		]
	])

	expected_g_biases = np.array(
	[
		[16.0],
		[-23.0]
	])

	l = ConvolutionalLayer((3, 7, 7), kernelSize=3, stride=2, numKernels=2)
	l.weights = weights
	l.biases = biases

	input = np.expand_dims(input, axis=0)
	forward_result = l.forward(input)
	error = forward_result
	error[0, 0, 0, 0] = 1
	error[0, 1, 0, 0] = 1

	backward_result = l.backward(input, error)
	new_weights = l.weights
	new_biases = l.biases
	g_weights = l.gradientWeights
	g_biases = l.gradientBiases
	return close_enough(weights, new_weights) and close_enough(biases, new_biases) and \
		close_enough(g_weights, expected_g_weights) and close_enough(g_biases, expected_g_biases) and \
		close_enough(backward_result, expected_backward_result)

def test_ConvolutionalLayer_complete_1():
	l = ConvolutionalLayer((1, 5, 5), kernelSize=3, stride=2, numKernels=1)

	input = np.array([
		[
			[0.0, 1.0, 2.0, 3.0, 4.0],
			[1.0, 2.0, 3.0, 4.0, 5.0],
			[2.0, 3.0, 4.0, 5.0, 6.0],
			[3.0, 4.0, 5.0, 6.0, 7.0],
			[4.0, 5.0, 6.0, 7.0, 8.0],
		]
	])

	weights = np.array([
		[
			[
				[0.5, 0.5, 0.5],
				[0.5, 0.5, 0.5],
				[0.5, 0.5, 0.5]
			]
		]
	])

	biases = np.array([[0.5]])
	l.weights = weights
	l.biases = biases

	expected_result_forward = np.array([
		[
			[9.5, 18.5],
			[18.5, 27.5]
		]
	])

	input = np.expand_dims(input, axis=0)
	result_forward = l.forward(input)
	errors = np.array([
		[
			[-1.0, 2.0],
			[3.0, 0.0]
		]
	])

	expected_result_backward = np.array([
		[
			[-0.5, -0.5, 0.5, 1.0, 1.0],
			[-0.5, -0.5, 0.5, 1.0, 1.0],
			[ 1.0,  1.0, 2.0, 1.0, 1.0],
			[ 1.5,  1.5, 1.5, 0.0, 0.0],
			[ 1.5,  1.5, 1.5, 0.0, 0.0]
		]
	])

	expected_gradient_weights = np.array([
		[
			[10.0, 14.0, 18.0],
			[14.0, 18.0, 22.0],
			[18.0, 22.0, 26.0]
		]
	])

	expected_gradient_biases = np.array([
		[4.0]
	])

	result_backward = l.backward(input, errors)
	return close_enough(expected_result_forward, result_forward) and \
		close_enough(expected_gradient_weights, l.gradientWeights) and \
		close_enough(expected_gradient_biases, l.gradientBiases) and \
		close_enough(expected_result_backward, result_backward)

from Mihlib import *
import numpy as np

def test_MaxPoolLayer_forward_1():
	input = np.array(
	[
		[
			[1, 0, 1, 2, 3, 1],
			[0, 0, 1, 1, 1, 1],
			[1, 0, 2, 0, 3, 1],
			[0, 0, 0, 0, 1, 1],
			[1, 0, 2, 1, 1, 1],
			[0, 0, 1, 1, 1, 3],
		]
	])

	expected_result_forward = np.array(
	[
		[
			[1, 2, 3],
			[1, 2, 3],
			[1, 2, 3]
		]
	])

	layer = MaxPoolLayer((1, 6, 6), stride=2)
	input = np.expand_dims(input, axis=0)
	result_forward = layer.forward(input)
	return close_enough(result_forward, expected_result_forward)

def test_MaxPoolLayer_backward_1():
	input = np.array(
	[
		[
			[1, 0, 1, 2, 3, 1],
			[0, 0, 1, 1, 1, 1],
			[1, 0, 2, 0, 3, 1],
			[0, 0, 0, 0, 1, 1],
			[1, 0, 2, 1, 1, 1],
			[0, 0, 1, 1, 1, 3],
		]
	])

	expected_result_backward = np.array(
	[
		[
			[1, 0, 0, 2, 3, 0],
			[0, 0, 0, 0, 0, 0],
			[1, 0, 2, 0, 3, 0],
			[0, 0, 0, 0, 0, 0],
			[1, 0, 2, 0, 0, 0],
			[0, 0, 0, 0, 0, 3],
		]
	])

	layer = MaxPoolLayer((1, 6, 6), stride=2)
	input = np.expand_dims(input, axis=0)
	result_forward = layer.forward(input)
	error = result_forward
	error[0, 0, 0, 0] = 1
	result_backward = layer.backward(input, error)
	
	return close_enough(expected_result_backward, result_backward)
import numpy as np
from mathematics.calculus.lucas_kanade import lucas_kanade_basic, lucas_kanade
from utilities.others import minMax

def test_lucas_kanade_basic_1():
	a, b = 0, 5
	expected_h = 0.01
	f = lambda x : np.sin(x)
	g = lambda x : f(x + expected_h)
	found_h = lucas_kanade_basic(f, g, a=a, b=b)
	return np.abs(expected_h - found_h) < 1e-5

def test_lucas_kanade_basic_2():
	a, b = minMax(*np.random.randint(-100, 100, size=(2, )))
	expected_h = 5 * (np.random.random() - 0.5)
	w1, w2, w3 = np.random.random(size=(3, )) - 0.5
	f = lambda x : w1 * x**2 + w2 * x + w3
	g = lambda x : f(x + expected_h)
	found_h = lucas_kanade_basic(f, g, a=a, b=b)
	return np.abs(expected_h - found_h) < 1e-5

def test_lucas_kanade_1():
	a, b = 0, 5
	expected_h = 0.01
	f = lambda x : np.sin(x)
	g = lambda x : f(x + expected_h)
	found_h = lucas_kanade(f, g, a=a, b=b)
	return np.abs(expected_h - found_h) < 1e-5

def test_lucas_kanade_2():
	a, b = minMax(*np.random.randint(-100, 100, size=(2, )))
	expected_h = 5 * (np.random.random() - 0.5)
	w1, w2, w3 = np.random.random(size=(3, )) - 0.5
	f = lambda x : w1 * x**2 + w2 * x + w3
	g = lambda x : f(x + expected_h)
	found_h = lucas_kanade(f, g, a=a, b=b)
	return np.abs(expected_h - found_h) < 1e-5


from utilities.transforms import Homogeneous
from utilities.public_api import *

def test_homogeneous_1():
	return Homogeneous() != None

def test_homogeneous_2():
	return Homogeneous(np.arange(16).reshape((4, 4))).matrix.shape == (4, 4)

def test_HTranslation_1():
	return close_enough(HTranslation().matrix, np.eye(4))

def test_HTranslation_2():
	a = HTranslation(9, 10, 11)
	b = np.array([[1, 0, 0, 9], [0, 1, 0, 10], [0, 0, 1, 11], [0, 0, 0, 1]])
	return close_enough(a.matrix, b)

def test_HRotationX_1():
	a = HRotationX(np.pi / 2)
	b = np.array([[1, 0, 0, 0], [0, 0, -1, 0], [0, 1, 0, 0], [0, 0, 0, 1]])
	return close_enough(a.matrix, b)

def test_HRotationY_1():
	a = HRotationY(np.pi / 2)
	b = np.array([[0, 0, 1, 0], [0, 1, 0, 0], [-1, 0, 0, 0], [0, 0, 0, 1]])
	return close_enough(a.matrix, b)

def test_HRotationZ_1():
	a = HRotationZ(np.pi / 2)
	b = np.array([[0, -1, 0, 0], [1, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
	return close_enough(a.matrix, b)

def test_multiple_transforms_1():
	a = HTranslation(1, 0, 0) * (HRotationX(np.pi / 2) * HTranslation(0, 1, 0))
	b = np.array([[1, 0, 0, 1], [0, 0, -1, 0], [0, 1, 0, 1], [0, 0, 0, 1]])
	return close_enough(a.matrix, b)

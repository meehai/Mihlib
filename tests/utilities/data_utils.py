from Mihlib import *

def test_toCategorical_1():
	a = [1, 2, 3, 2, 1]
	b = toCategorical(a)
	expected_b = [ [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 0] ]
	return close_enough(b, expected_b)

def test_toCategorical_2():
	a = [1, 2, 3, 2, 1]
	b = toCategorical(a, numClasses=6)
	expected_b = [ [0, 1, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0], [0, 0, 0, 1, 0, 0], [0, 0, 1, 0, 0, 0], [0, 1, 0, 0, 0, 0] ]
	return close_enough(b, expected_b)

def test_normalizeData_1():
	data = np.array([1, 2, 3, 2, 1, 2, 3, 2, 1, 20], dtype="float")
	newData = normalizeData(data, np.mean(data), np.std(data))
	return close_enough(np.mean(newData), 0) and close_enough(np.std(newData), 1)

def test_borderData_1():
	data = np.zeros((500, 28, 28, 3))
	newData = borderData(data, (32, 32, 3))
	return newData.shape == (500, 32, 32, 3)

def test_borderData_2():
	data = np.zeros((500, 1, 28, 28))
	newData = borderData(data, (3, 35, 32))
	return newData.shape == (500, 3, 35, 32)

def test_reshapeData_1():
	data = np.zeros((500, 20, 20, 3))
	newData = reshapeData(data, (400, 3))
	return newData.shape == (500, 400, 3)

def test_isotropicNormalization_1():
	data = np.random.normal(size=(100, 2))
	newData, T = isotropicNormalizationData(data)
	return close_enough((T @ toHomogeneousData(data).T).T, toHomogeneousData(newData))
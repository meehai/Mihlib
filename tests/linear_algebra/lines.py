from Mihlib import *

def test_getLineParams_1():
	p1 = (1, 3)
	p2 = (3, 5)
	a, b, c = getLineParams(p1, p2)
	return a == 2 and b == -2 and c == 4

def test_getLineParams_2():
	p1 = (3, 5)
	p2 = (3, 10)
	a, b, c = getLineParams(p1, p2)
	return a == 5 and b == 0 and c == -15

def test_getLineParams_3():
	p1 = (3, 5)
	p2 = (10, 5)
	a, b, c = getLineParams(p1, p2)
	return a == 0 and b == -7 and c == 35

def test_getLineParams_mb_1():
	p1 = (1, 3)
	p2 = (3, 5)
	m, b = getLineParams_mb(p1, p2)
	return m == 1 and b == 2

def test_getIntersectionPoint_mb_1():
	l1 = getLineParams_mb((1,3), (3,5))
	l2 = getLineParams_mb((5, 5), (7, 10))
	p = getIntersectionPoint_mb(l1, l2)
	return close_enough(p[X], 6.3333) and close_enough(p[Y], 8.3333)

def test_getIntersectionPoint_1():
	X1 = uniqueRandint(-10, 10)
	X2 = uniqueRandint(-10, 10)
	p1, p2 = (-10, X1), (5, X1)
	horizontalLine = getLineParams(p1, p2)

	p3, p4 = (X2, 3), (X2, 10)
	verticalLine = getLineParams(p3, p4)

	intersectionPoint1 = getIntersectionPoint(verticalLine, horizontalLine)
	intersectionPoint2 = getIntersectionPoint(horizontalLine, verticalLine)
	return intersectionPoint1 == intersectionPoint2

def test_isPointOnLine_1():
	p1 = (3, 3)
	p2 = (5, 5)
	line = getLineParams(p1, p2)
	return isPointOnLine(line, (4, 4)) == True and isPointOnLine(line, (1, 1)) == True \
		and isPointOnLine(line, (4, 5)) == False

def test_isPointOnLine_2():
	p1 = (3, 5)
	p2 = (3, 10)
	line = getLineParams(p1, p2)
	return isPointOnLine(line, (3, 6)) == True and isPointOnLine(line, (3, 100)) == True

def test_isPointOnSegment_1():
	p1 = (3, 3)
	p2 = (5, 5)
	return isPointOnSegment(p1, p2, (4, 4)) == True and isPointOnSegment(p1, p2, (1, 1)) == False \
		and isPointOnSegment(p1, p2, (4, 5)) == False

def test_isPointOnSegment_2():
	p1 = (3, 5)
	p2 = (3, 10)
	return isPointOnSegment(p1, p2, (3, 6)) == True and isPointOnSegment(p1, p2, (3, 100)) == False

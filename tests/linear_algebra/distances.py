from Mihlib import *

def test_euclidean_distance_1():
	p1 = (1, 2)
	p2 = (3, 2)
	return euclidean_distance(p1, p2) == 2

def test_manhattan_distance_1():
	p1 = (10, 15)
	p2 = (15, 10)
	return manhattan_distance(p1, p2) == 10

def test_distance_point_line_1():
	p1, p2 = (1, 1), (3, 3)
	p = (8, 12)
	return close_enough(distance_point_line(p1, p2, p), 2.8284)
